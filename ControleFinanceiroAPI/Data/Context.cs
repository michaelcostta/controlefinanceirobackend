﻿using Microsoft.EntityFrameworkCore;
using ControleFinanceiroAPI.Models;

namespace ControleFinanceiroAPI.Data
{
	public class Context : DbContext
	{
		public Context(DbContextOptions<Context> opt) : base(opt)
		{

		}

		public DbSet<Lancamento> Lancamentos { get; set; }
	}
}
