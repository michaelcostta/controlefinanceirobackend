﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ControleFinanceiroAPI.Data.Dtos
{
	public class ReadLancamentoDto
	{
		[Key]
		[Required]
		public int Id { get; set; }
		[Required]
		[StringLength(30)]
		public string Titulo { get; set; }
		[Required]
		[StringLength(300)]
		public string Descricao { get; set; }
		[Range(1, 1000000)]
		public double Valor { get; set; }
		[Required]
		public DateTime Data { get; set; }
		[Required]
		public bool EhReceita { get; set; }
	}
}