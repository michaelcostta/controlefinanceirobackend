﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ControleFinanceiroAPI.Data;
using ControleFinanceiroAPI.Data.Dtos;
using ControleFinanceiroAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace ControleFinanceiroAPI.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class LancamentoController : ControllerBase
	{
		private Context _context;
		private IMapper _mapper;

		public LancamentoController(Context context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		[HttpPost]
		public IActionResult SetLancamento([FromBody] CreateLancamentoDto LancamentoDto)
		{
			Lancamento lancamento = _mapper.Map<Lancamento>(LancamentoDto);

			_context.Lancamentos.Add(lancamento);
			_context.SaveChanges();
			return CreatedAtAction(nameof(GetLancamentoId), new { Id = lancamento.Id }, lancamento);
		}

		[HttpGet]
		public IEnumerable<Lancamento> GetAllLancamento()
		{
			return _context.Lancamentos;
		}

		[HttpGet("{id}")]
		public IActionResult GetLancamentoId(int id)
		{
			Lancamento lancamento = _context.Lancamentos.FirstOrDefault(lancamento => lancamento.Id == id);
			if (lancamento != null)
			{
				ReadLancamentoDto LancamentoDto = _mapper.Map<ReadLancamentoDto>(lancamento);

				return Ok(LancamentoDto);
			}
			return NotFound();
		}

		[HttpPut("{id}")]
		public IActionResult UpdateLancamento(int id, [FromBody] UpdateLancamentoDto lancamentoDto)
		{
			Lancamento lancamento = _context.Lancamentos.FirstOrDefault(lancamento => lancamento.Id == id);
			if (lancamento == null)
			{
				return NotFound();
			}

			_mapper.Map(lancamentoDto, lancamento);
			_context.SaveChanges();
			return NoContent();
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteLancamento(int id)
		{
			Lancamento lancamento = _context.Lancamentos.FirstOrDefault(lancamento => lancamento.Id == id);
			if (lancamento == null)
			{
				return NotFound();
			}
			_context.Remove(lancamento);
			_context.SaveChanges();
			return NoContent();
		}
	}
}