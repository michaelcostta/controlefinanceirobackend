﻿using AutoMapper;
using ControleFinanceiroAPI.Data.Dtos;
using ControleFinanceiroAPI.Models;

namespace ControleFinanceiroAPI.Profiles
{
	public class LancamentoProfile : Profile
	{
		public LancamentoProfile()
		{
			CreateMap<CreateLancamentoDto, Lancamento>();
			CreateMap<Lancamento, ReadLancamentoDto>();
			CreateMap<UpdateLancamentoDto, Lancamento>();
		}
	}
}